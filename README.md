### 使用说明请查看官方文档
[官方文档](http://wayla.gitee.io/compage "官方文档")

### 客户使用案例
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210408172547410.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzI3NTMyMTY3,size_16,color_FFFFFF,t_70#pic_center)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210408172557687.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzI3NTMyMTY3,size_16,color_FFFFFF,t_70#pic_center)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210408172605307.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzI3NTMyMTY3,size_16,color_FFFFFF,t_70#pic_center)
![在这里插入图片描述](https://img-blog.csdnimg.cn/2021040817261313.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzI3NTMyMTY3,size_16,color_FFFFFF,t_70#pic_center)

### 实例方法

```javascript
instance.close() //关闭当前弹框
instance.min() //最小化当前弹框
instance.max() //最大化当前弹框
instance.reset() //窗口复原
....更多接口可查看instance实例对象
```
****
### 默认效果图
![回调函数演示](https://img-blog.csdnimg.cn/20210402234803754.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzI3NTMyMTY3,size_16,color_FFFFFF,t_70#pic_center)
![多弹框测试](https://img-blog.csdnimg.cn/20210402235134408.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzI3NTMyMTY3,size_16,color_FFFFFF,t_70#pic_center)

### 扩展
**重要的事情说三遍**
**重要的事情说三遍**
**重要的事情说三遍**
###
其中 mixin 参数可搭配自定义组件实现和自定义主题实现完全个人化制定弹框
**如果不了解vue的mixin的用法，可以先查看[官方文档](https://cn.vuejs.org/v2/api/#Vue-mixin)**
###
初始化 Vue.use(layer,option); 其中 option 可以设置全局弹框的配置项，
如个别弹框有特殊配置项，在调用 vm.\$layer.open(option);时可以指定覆盖
****
##  联系，问题反馈
QQ群：450342630
