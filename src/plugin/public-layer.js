import "./base.css";
import {
  vueAttribute,
  dialogTemplate,
  getheadHtml,
  getbodyHtml,
  shadeTemplate,
  uuid,
  dataUtils,
} from "./tool";

//小驼峰命名规则
//默认选项
var defaultOption = {
  hasHead: true, //是否拥有头部
  customHead: null, //自定义头部（支持组件、html代码片段、dom元素）
  title: "标题", //标题
  hasMin: true, //是否有最小化按钮
  hasMax: true, //是否有最大化按钮
  content: null, //弹框内容（支持组件、html代码片段、dom元素）
  move: true, //是否可以拖动
  shade: false, //是否有遮罩
  minWidth: 300, //最小化宽度
  minHight: 40, //最小化高度
  bottomOff: 30, //最小化后距离窗口底部距离
  leftOff: 4, //最小化后距离窗口左边距离
  rowGap: 8, //行间距
  colGap: 8, //列间距
  location: {
    //弹框位置，可由对象指定,为空则默认居中显示
    top: null,
    left: null,
    bottom: null,
    right: null,
  },
  area: {
    //弹框大小
    height: "300px",
    width:"300px",
  },
  subData: null, //用于弹框接受参数
  multiple: true, //是否为多弹框模式
  blank: false, //是否在点击空白处自动关闭弹框，仅在shade为true时有效
  mixin: null, //混淆，用于完全个性化制定
  theme: null, //自定义皮肤
  //回调开始
  success: null, //弹出成功后回调type: Function
  cancel: null, //弹框关闭回调，type: Function
  minBack: null, //最小化回调type: Function
  maxBack: null, //最大化回调type: Function
  restore: null, //复原后回调
};
var layer = {
  install: function(Vue, options) {
    options = options || {};
    //默认项合并
    defaultOption = { ...defaultOption, ...options };
    var layer = {
      open: function(option) {
        option = option || {};
        defaultOption.Vue = Vue;
        defaultOption.uuid = uuid();
        //合并选项
        option = { ...defaultOption, ...option };
        var headHtml = getheadHtml(option);
        var bodyHtml = getbodyHtml(option);
        var shadeHtml = shadeTemplate(option.shade);
        var dynamic_Layer = Vue.extend({
          template: dialogTemplate(headHtml + bodyHtml + shadeHtml),
          ...vueAttribute(option),
        });

        const instance = new dynamic_Layer().$mount();
        document.body.appendChild(instance.$el);
        return instance; //返回实例
      },
      close: function(instance) {
        var parent = instance.$el.parentNode;
        if (parent) {
          parent.removeChild(instance.$el);
        }
      },
      closeAll: function() {
        dataUtils.deleteAll();
      },
    };
    Vue.prototype.$layer = layer; //挂在到vue原型
  },
};


if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(layer);
}


export default layer;
