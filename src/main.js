import Vue from 'vue'
import App from './App.vue'

//import layer from 'vue-pubilc-layer'
import layer from './plugin/public-layer'
Vue.use(layer,{
  hasMin:true,
  hasMax:true
});

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
