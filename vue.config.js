module.exports = {

    // 是否开启eslint保存检测，有效值：ture | false | 'error'
    lintOnSave: true,
    runtimeCompiler: true
}